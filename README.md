# README #

### ¿Para qué es este repositorio? ###

* Este repositorio contiene el fichero base para desarrollar la librería "libberry" para las prácticas en ensamblador ARM con la Raspberry de la asignatura de Tecnología de Computadores de los Grados en Informática de la Universidad de Málaga

### ¿Que hacer con ellos? ###

* Descargarlos y programar las funciones que se indican en el guión de prácticas en el fichero libberry.s.
* Ensamblar la librería una vez implementadas las funciones: as libberry.s -o libberry.o
* Para probar la función "setLeds", enlaza la librería con el fichero objeto pruLedsLibBerry.o proporcionado: gcc pruLedsLibBerry.o libberry.o -lwiringPi
* Para probar la función "playNote", enlaza la librería con el fichero objeto pruSoundLibBerry.o proporcionado: gcc pruSoundLibBerry.o libberry.o -lwiringPi